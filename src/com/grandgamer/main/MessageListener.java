import net.dv8tion.jda.entities.User;
import net.dv8tion.jda.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.hooks.ListenerAdapter;

import java.util.ArrayList;

/**
 * Created by grandgamer on 24/4/2016.
 */
public class MessageListener extends ListenerAdapter {

    /**
     * Direct/Private Message received.
     *
     * @param event the Message Event.
     */
    @Override
    public void onPrivateMessageReceived(PrivateMessageReceivedEvent event) {
        if(event.getAuthor().getDiscriminator() != event.getJDA().getSelfInfo().getDiscriminator()) {
            System.out.println("Hello " + event.getAuthor().getUsername());
            event.getChannel().sendMessage("Hello " + event.getAuthor().getAsMention());
        }
    }

    /**
     * Channel message received.
     *
     * @param event the Message Event
     */
    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String mes = event.getMessage().getStrippedContent();
        User self = event.getJDA().getSelfInfo();
        ArrayList<User> mentionedUsers = new ArrayList<User>(event.getMessage().getMentionedUsers());

        //Command check
        if(mes.startsWith("!"))
            System.out.printf("Command");

        //Self mention check
        if(mentionedUsers.contains(self))
            event.getChannel().sendMessage("You called?");
    }
}
